package com.ilkeiapps.ilkeitool.test;

import com.ilkeiapps.ilkeitool.object.IlKeiCopyBean;
import com.ilkeiapps.ilkeitool.test.bean.BeanA;
import com.ilkeiapps.ilkeitool.test.bean.BeanB;
import org.junit.jupiter.api.Test;

public class BeanCopyTest {

    @Test
    public void testCopy() {

        var a = BeanA.builder().fb(12l).fa(21l).da("342").db("eeqw").build();
        var b = new BeanB();

        IlKeiCopyBean.builder().withSource(a).withDestination(b).execute();

        System.out.println(b);

        b = new BeanB();

        IlKeiCopyBean.builder().withSource(a).withDestination(b).excludeField("db").excludeField("fb").execute();

        System.out.println(b);

        b = new BeanB();
        b.setFb(8888L);

        IlKeiCopyBean.builder().withSource(a).withDestination(b).excludeField("db").excludeField("fb").useField("da").execute();

        System.out.println(b);
    }
}
