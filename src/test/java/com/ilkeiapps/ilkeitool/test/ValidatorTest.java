package com.ilkeiapps.ilkeitool.test;

import com.ilkeiapps.ilkeitool.test.bean.ValidatorBean;
import com.ilkeiapps.ilkeitool.validator.IlKeiValidator;
import org.junit.jupiter.api.Test;

public class ValidatorTest {

    @Test
    public void testValidator() {
        System.out.println("oi");

        var tmp = ValidatorBean.builder()
                .id(3L)
                .name("Anak Ku")
                .salary(67000000D)
                .build();

        var pm = IlKeiValidator.builder().withPojo(tmp)
                .pick("id").withLabel("Id").isMandatory().asLong().withMinLen(5).pack()
                .pick("name").isMandatory().asString().withMinLen(2).withMaxLen(6).pack()
                .validate();

        if (pm.getResult() == false) {
            System.out.println(pm.getMessage());
        }

    }
}
