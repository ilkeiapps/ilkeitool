package com.ilkeiapps.ilkeitool.test.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class BeanA {

    private Long fa;

    private Long fb;

    private String da;

    private String db;
}
