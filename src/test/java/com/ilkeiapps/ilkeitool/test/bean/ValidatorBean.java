package com.ilkeiapps.ilkeitool.test.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@Builder
public class ValidatorBean {

    private Long id;

    private String name;

    private Double salary;

    private Integer no;

    private Date dob;
}
