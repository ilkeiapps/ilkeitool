package com.ilkeiapps.ilkeitool.test.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString(includeFieldNames = true)
public class BeanB {

    private Long fa;

    private Long fb;

    private String da;

    private String db;
}
