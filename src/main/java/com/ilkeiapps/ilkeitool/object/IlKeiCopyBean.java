package com.ilkeiapps.ilkeitool.object;

import jodd.bean.BeanCopy;
import jodd.bean.BeanUtilBean;

import java.lang.reflect.Field;
import java.util.*;

public class IlKeiCopyBean  implements  ICopyBean {

    private Object source;
    private Object destination;

    private List<String> selected;

    private List<String> exclude;

    private static IlKeiCopyBean instance;

    public static IlKeiCopyBean builder() {
        instance = new IlKeiCopyBean();
        return instance;
    }


    private IlKeiCopyBean() {
        this.exclude = new ArrayList<>();
        this.selected = new ArrayList<>();
    }

    private boolean allFields = true;

    @Override
    public ICopyBean withSource(Object src) {
        this.source = src;
        return this;
    }

    @Override
    public ICopyBean withDestination(Object src) {
        this.destination = src;
        return this;
    }

    @Override
    public ICopyBean excludeField(String field) {
        this.exclude.add(field);
        return this;
    }

    @Override
    public ICopyBean useField(String field) {
        this.selected.add(field);
        this.allFields = false;
        return this;
    }

    @Override
    public void execute() {
        // TODO Auto-generated method stub
        if (this.source == null || this.destination == null) {
            return;
        }


        Map<String, String> map = new HashMap<String, String>();
        if (this.exclude != null) {
            for (String string : exclude) {
                map.put(string, string);
            }
        }

        if (this.allFields) {
            Field[] fds = this.source.getClass().getDeclaredFields();
            for (Field field : fds) {
                if (field.getName().equalsIgnoreCase("serialVersionUID")) {
                    continue;
                }

                this.doCopyField(map, field);
            }
        } else {

            for (String fieldNane: this.selected) {
                try {
                    Field field = this.destination.getClass().getDeclaredField(fieldNane);

                    this.doCopyField(map, field);

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }

    }

    private void doCopyField(Map<String, String> map, Field field) {

        try {

            if (map.containsKey(field.getName())) {
                return;
            }
            if (BeanUtilBean.declared.hasProperty(this.destination, field.getName()) == false) {
                return;
            }

            Field destField = this.destination.getClass().getDeclaredField(field.getName());
            Class<?> destClz = destField.getType();
            Class<?> clz = field.getType();

            if (destClz.isAssignableFrom(clz) == false) {
                return;
            }

            if (List.class.isAssignableFrom(clz)) {
                return;
            }
            if (Set.class.isAssignableFrom(clz)) {
                return;
            }
            Object o = BeanUtilBean.declared.getProperty(this.source, field.getName());
            if (o != null) {
                BeanUtilBean.declared.setProperty(this.destination, field.getName(), o);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }
}
