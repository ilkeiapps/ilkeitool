package com.ilkeiapps.ilkeitool.object;

public interface ICopyBean {

    public ICopyBean withSource(Object src);

    public ICopyBean withDestination(Object src);

    public ICopyBean excludeField(String field);

    public ICopyBean useField(String field);

    public void execute();
}
