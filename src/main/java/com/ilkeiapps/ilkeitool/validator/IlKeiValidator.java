package com.ilkeiapps.ilkeitool.validator;

public class IlKeiValidator implements IValidator {

    private static IlKeiValidator instance = null;

    private ValidationContent val = null;

    private IlKeiValidator() {
        // TODO Auto-generated constructor stub
    }

    public static IlKeiValidator builder() {
        instance = new IlKeiValidator();
        return instance;
    }

    @Override
    public IValidationContent withPojo(Object obj) {
        // TODO Auto-generated method stub
        val = new ValidationContent(ValidationDataSource.POJO);
        val.setTargetObj(obj);
        return val;
    }
}
