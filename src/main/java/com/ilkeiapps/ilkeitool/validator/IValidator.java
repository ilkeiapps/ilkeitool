package com.ilkeiapps.ilkeitool.validator;

public interface IValidator {

    IValidationContent withPojo(Object obj);

}
