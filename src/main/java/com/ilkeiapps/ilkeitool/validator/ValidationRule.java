package com.ilkeiapps.ilkeitool.validator;

import jodd.bean.BeanUtilBean;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class ValidationRule implements IValidatorRule {

    private ValidationContent content;
    private String fieldName = null;
    private String fieldLabel = "";
    private boolean isMandatory = false;
    private boolean isString = false;
    private boolean isInteger = false;
    private boolean isLong = false;
    private boolean isDate = false;
    private boolean isDouble = false;
    private int min = -987654321;
    private int max = -987654321;
    private String dateFormat = "yyyy-MM-dd";
    private String lang = "id";

    private BeanUtilBean bb;

    public ValidationRule(ValidationContent content, String name, String label) {
        // TODO Auto-generated constructor stub
        this.content = content;
        this.fieldName = name;
        this.fieldLabel = label;
        this.bb = new BeanUtilBean();
    }

    public ValidationRule(ValidationContent content, String name) {
        // TODO Auto-generated constructor stub
        this.content = content;
        this.fieldName = name;
        this.fieldLabel = name;
        this.bb = new BeanUtilBean();
    }

    @Override
    public IValidatorRule isMandatory() {
        // TODO Auto-generated method stub
        this.isMandatory = true;
        return this;
    }

    @Override
    public IValidatorRule asString() {
        // TODO Auto-generated method stub
        this.isString = true;
        return this;
    }

    @Override
    public IValidationContent pack() {
        // TODO Auto-generated method stub
        return this.content;
    }

    @Override
    public IValidatorRule asDate() {
        // TODO Auto-generated method stub
        this.isDate = true;
        return this;
    }

    @Override
    public IValidatorRule asInteger() {
        // TODO Auto-generated method stub
        this.isInteger = true;
        return this;
    }

    @Override
    public IValidatorRule asLong() {
        // TODO Auto-generated method stub
        this.isLong = true;
        return this;
    }

    @Override
    public IValidatorRule asDouble() {
        // TODO Auto-generated method stub
        this.isDouble = true;
        return this;
    }

    @Override
    public IValidatorRule withMinLen(Integer len) {
        // TODO Auto-generated method stub
        this.min = len.intValue();
        return this;
    }

    @Override
    public IValidatorRule withMaxLen(Integer len) {
        // TODO Auto-generated method stub
        this.max = len.intValue();
        return this;
    }

    @Override
    public IValidatorRule withDateFormat(String format) {
        // TODO Auto-generated method stub
        this.dateFormat = format;
        return this;
    }

    @Override
    public IValidatorRule withLabel(String name) {
        // TODO Auto-generated method stub
        this.fieldLabel = name;
        return this;
    }

    public ProcessResult validate() {
        return this.validatePojo(this.lang);
    }

    private ProcessResult check(ProcessResult pr, String field, String lang) {
        if (this.isMandatory) {
            if (field == null || field.equals("")) {
                if (lang.equals("id"))
                    pr.setMessage(this.fieldLabel + " harus diisi");
                else
                    pr.setMessage(this.fieldLabel + " is mandatory");

                return pr;
            }
        }
        if (this.isString) {
            if (this.min != -987654321) {
                if (field.length() < this.min) {
                    if (lang.equals("id"))
                        pr.setMessage(this.fieldLabel + " panjang minimum adalah "+ this.min);
                    else
                        pr.setMessage(this.fieldLabel + " length minimum is " + this.min);
                    return pr;
                }
            }
            if (this.max != -987654321) {
                if (field.length() > this.max) {
                    if (lang.equals("id"))
                        pr.setMessage(this.fieldLabel + " panjang maksimum adalah "+ this.max);
                    else
                        pr.setMessage(this.fieldLabel + " length maximun is " + this.max);

                    return pr;
                }
            }
        }
        if (this.isInteger) {
            try {
                int tmp = Integer.parseInt(field);
                if (this.min != -987654321) {
                    if (tmp < this.min) {
                        if (lang.equals("id"))
                            pr.setMessage(this.fieldLabel + " panjang minimum adalah "+ this.min);
                        else
                            pr.setMessage(this.fieldLabel + " length minimum is " + this.min);

                        return pr;
                    }
                }
                if (this.max != -987654321) {
                    if (tmp > this.max) {
                        if (lang.equals("id"))
                            pr.setMessage(this.fieldLabel + " panjang maksimum adalah "+ this.max);
                        else
                            pr.setMessage(this.fieldLabel + " length maximun is " + this.max);

                        return pr;
                    }
                }
            } catch (Exception e) {
                if (this.isMandatory) {
                    if (lang.equals("id"))
                        pr.setMessage(this.fieldLabel + " harus integer");
                    else
                        pr.setMessage(this.fieldLabel + " must an integer");

                    return pr;
                }
            }
        }
        if (this.isLong) {
            try {
                long tmp = Long.parseLong(field);
                if (this.min != -987654321) {
                    if (tmp < this.min) {
                        if (lang.equals("id"))
                            pr.setMessage(this.fieldLabel + " panjang minimum adalah "+ this.min);
                        else
                            pr.setMessage(this.fieldLabel + " length minimum is " + this.min);

                        return pr;
                    }
                }
                if (this.max != -987654321) {
                    if (tmp > this.max) {
                        if (lang.equals("id"))
                            pr.setMessage(this.fieldLabel + " panjang maksimum adalah "+ this.max);
                        else
                            pr.setMessage(this.fieldLabel + " length maximun is " + this.max);

                        return pr;
                    }
                }
            } catch (Exception e) {
                if (this.isMandatory) {
                    if (lang.equals("id"))
                        pr.setMessage(this.fieldLabel + " harus type long");
                    else
                        pr.setMessage(this.fieldLabel + " must a long");

                    return pr;
                }
            }
        }
        if (this.isDate) {
            try {
                Locale lo = new Locale("id", "ID");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(this.dateFormat, lo);
                LocalDate dateTime = LocalDate.parse(field, formatter);
                if (dateTime == null) {
                    if (lang.equals("id"))
                        pr.setMessage(this.fieldLabel + " harus tanggal");
                    else
                        pr.setMessage(this.fieldLabel + " must a date");

                    return pr;
                }
            } catch (Exception e) {
                if (this.isMandatory) {
                    if (lang.equals("id"))
                        pr.setMessage(this.fieldLabel + " harus tanggal");
                    else
                        pr.setMessage(this.fieldLabel + " must a date");

                    return pr;
                }

            }
        }

        pr.setResult(true);
        return pr;
    }

    private ProcessResult validatePojo(String lang) {
        ProcessResult pr = new ProcessResult();
        pr.setResult(false);
        Object obj = this.content.getTargetObj();
        String field = null;

        if (this.isDate) {
            Object tmp = BeanUtilBean.declared.getProperty(obj, this.fieldName);
            Date dt = (Date) tmp;
            SimpleDateFormat sdf = new SimpleDateFormat(this.dateFormat);
            field = sdf.format(dt);
        } else {
            Object stmp = null;
            try {
                stmp = BeanUtilBean.declared.getProperty(obj, this.fieldName);
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            if (stmp != null) {
                field = stmp.toString();
            }
        }

        return check(pr, field, lang);
    }
}
