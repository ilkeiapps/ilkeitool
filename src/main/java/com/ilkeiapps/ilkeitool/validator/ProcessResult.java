package com.ilkeiapps.ilkeitool.validator;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProcessResult {

    private Boolean result;
    private String message;


}
