package com.ilkeiapps.ilkeitool.validator;

public interface IValidationContent {

    public IValidatorRule pick(String name, String label);
    public IValidatorRule pick(String name);

    public ProcessResult validate();

}
