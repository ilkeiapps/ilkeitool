package com.ilkeiapps.ilkeitool.validator;

public interface IValidatorRule {

    public IValidatorRule isMandatory();
    public IValidatorRule asString();
    public IValidatorRule asDate();
    public IValidatorRule asInteger();
    public IValidatorRule asLong();
    public IValidatorRule asDouble();
    public IValidatorRule withMinLen(Integer len);
    public IValidatorRule withMaxLen(Integer len);
    public IValidatorRule withDateFormat(String format);
    public IValidatorRule withLabel(String name);


    public IValidationContent pack();


}
