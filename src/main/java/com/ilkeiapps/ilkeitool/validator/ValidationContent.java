package com.ilkeiapps.ilkeitool.validator;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class ValidationContent implements IValidationContent {

    private List<ValidationRule> rules = null;

    @Getter
    private ValidationDataSource valDataSource = null;

    @Setter
    @Getter
    private Object targetObj;

    public ValidationContent(ValidationDataSource valDataSource) {
        // TODO Auto-generated constructor stub
        rules = new ArrayList<>();
        this.valDataSource = valDataSource;
    }

    @Override
    public IValidatorRule pick(String name, String label) {
        // TODO Auto-generated method stub
        ValidationRule rule = new ValidationRule(this, name, label);
        rules.add(rule);
        return rule;
    }

    @Override
    public IValidatorRule pick(String name) {
        // TODO Auto-generated method stub
        ValidationRule rule = new ValidationRule(this, name);
        rules.add(rule);
        return rule;
    }

    @Override
    public ProcessResult validate() {
        // TODO Auto-generated method stub
        for (ValidationRule validationRule : rules) {
            ProcessResult pr = validationRule.validate();
            if (pr.getResult().booleanValue() == false) {
                return pr;
            }
        }
        ProcessResult pr = new ProcessResult();
        pr.setResult(true);
        pr.setMessage("");
        return pr;
    }
}
